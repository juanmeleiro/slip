BIN_PATH=/usr/bin
MAN_PATH=/usr/share/man/man1
MARKDOWN_PROCESSOR=smu
CC=cc

slip : slip.c
	$(CC) slip.c -o slip

all : slip docs.html

docs.html : slip docs/docs.slip docs/header.html docs/footer.html
	./slip merge docs/docs.slip > docs.md
	$(MARKDOWN_PROCESSOR) docs.md > body.html
	cat docs/header.html body.html docs/footer.html > docs.html

clean :
	rm -f docs.md body.html slip docs.html slip.1.gz

install : slip
	cp slip $(BIN_PATH)/slip
	gzip --keep --force slip.1
	cp slip.1.gz $(MAN_PATH)

dist :
	pijul dist -d slip-$(shell date +%Y-%m-%dT%H-%M-%S)
