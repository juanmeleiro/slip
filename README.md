# slip – simple literate programming

## Building

```sh
$ make
$ sudo make install
```

## Documentation

Please se `slip(1)` for usage information. For documentation of source, see
`docs.html` after building.
