/* @ main.argstring */
#define MAIN_ARG_STRING     "+h"
/* @ end */
#define MERGE_ARG_STRING    "o:"
#define BLOCKS_ARG_STRING   "o:"
#define INITIAL_BUFFER_SIZE 256
#define CONFIG_FILE_NAME    "slip.config"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_RESET   "\x1b[0m"

typedef struct Config {
  char *block_start_prefix;
  char *block_end;
  char *insertion_point_marker;
} Config;

static Config *config;

typedef struct Codeblock {
  char *id; /* Null-terminated string */
  size_t size;
  char *code; /* Null-terminated string */
  unsigned int indent;
  unsigned int line;
} Codeblock;

typedef struct Codefile {
  char *path; /* Null-terminated string */
  unsigned int size; /* Allocated size at blocks */
  unsigned int items; /* Number of occupied positions in blocks */
  Codeblock *blocks;
} Codefile;

/* @ Codebase */
typedef struct Codebase {
  unsigned int size; /* Allocated memory at pointer files */
  unsigned int items; /* Number of occupied positions from pointer files */
  Codefile *files;
} Codebase;
/* @ end */

typedef enum
Commands {
  MERGE,
  BLOCKS,
  UNKNOWN
} Command;

/* Main and subcommands */
int main (int argc, char **argv);
int merge (int argc, char **argv);
int blocks (int argc, char **argv);

/* Config and commandline parsing */
Config* parseconfig (FILE *f);
Command getsubcommand (char *cmd);

/* Parsing basics */
char* token (FILE *f);
unsigned int scanuntil (FILE *source, const char *pattern, FILE *sink);
unsigned int whitespace (FILE *f);
int endline (FILE *f);

/* Parsing */
Codefile* loadcode (char *filename);

/* Getters */
Codeblock* getblockinfile (Codefile *file, char *id);
Codeblock* getblockinbase (Codebase *base, char *path, char *id);
Codefile* getfileinbase (Codebase *base, char *path);

/* Setters */
void appendtocodebase (Codebase *base, Codefile *file);
void appendtocodefile (Codefile *file, Codeblock *block);
void computeindent (Codeblock *block);

/* Printers */
void fprintbase (FILE *f, Codebase *base);
void fprintblock (FILE *f, Codeblock *block);
void fprintfile (FILE *f, Codefile *file);
