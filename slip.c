#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>

#include "slip.h"

void
die(const char *errstr, ...)
{
	va_list ap;

	va_start(ap, errstr);
	fputs(ANSI_COLOR_RED, stderr);
	vfprintf(stderr, errstr, ap);
	fputs(ANSI_COLOR_RESET, stderr);
	va_end(ap);
}

Config*
parseconfig(FILE *f) {
  size_t s;
  Config *config = malloc(sizeof(Config));

  config->block_start_prefix     = NULL;
  config->block_end              = NULL;
  config->insertion_point_marker = NULL;

  s = 0;
  s = getline(&(config->block_start_prefix), &s, f);
  if (config->block_start_prefix[s-1] == '\n') config->block_start_prefix[s-1] = '\0';

  s = 0;
  s = getline(&(config->block_end), &s, f);
  if (config->block_end[s-1] == '\n') config->block_end[s-1] = '\0';

  s = 0;
  s = getline(&(config->insertion_point_marker), &s, f);
  if (config->insertion_point_marker[s-1] == '\n') config->insertion_point_marker[s-1] = '\0';

  return config;
}

void
fprintblock(FILE *f, Codeblock *block)
{
  fputs(block->code, f);
}

void
fprintfile(FILE *f, Codefile* file)
{
  int i;
  for (i = 0; i < file->items; i++) {
    fprintf(f, "[BLOCK %s]\n", file->blocks[i].id);
    fprintblock(f, &(file->blocks[i]));
    fprintf(f, "[END]\n");
  }
}

void
fprintbase(FILE* f, Codebase* base)
{
  int i;
  for (i = 0; i < base->items; i++) {
    fprintf(f, "[FILE %s]\n", base->files[i].path);
    fprintfile(f, &(base->files[i]));
  }
}

unsigned int
scanuntil(FILE *source, const char* pattern, FILE *sink)
{
  /* Scan FILE `source` until an instance of `pattern` is found or
  `EOF` is reached. Characters which are scanned but are not part of
  such an instance are printed to FILE `sink`.*/
  int progress = 0, lines = 0;
  char c;

  while (pattern && pattern[progress]) {
    c = fgetc(source);
    lines += (c == '\n');
    if (c == EOF) {
      sink && fprintf(sink, "%.*s", progress, pattern);
      break;
    } else if (c == pattern[progress]) {
      progress++;
    } else {
      sink && fprintf(sink, "%.*s%c", progress, pattern, c);
      progress = 0;
    }
  }
  return lines;
}

unsigned int
whitespace(FILE *f)
{
  char c, lines = 0;
  do {
    c = fgetc(f);
    lines += (c == '\n');
  } while (isblank(c));
  ungetc(c, f);
  return lines;
}

char*
token(FILE *f)
{
  unsigned int count = 0, buffer_length = INITIAL_BUFFER_SIZE;
  char c, *buffer = malloc(buffer_length*sizeof(char));

  for (c = fgetc(f); !isspace(c) && c != EOF; c = fgetc(f)) {
    buffer[count] = c;
    count++;
    if (count == buffer_length) {
      buffer_length *= 2;
      buffer = realloc(buffer, buffer_length*sizeof(char));
    }
  }
  buffer[count] = '\0';

  return buffer;
}

int
endline(FILE *f)
{
  char c;
  do {
    c = fgetc(f);
  } while (c != '\n' && c != EOF);
  return (c == EOF);
}

void
appendtocodefile(Codefile* file, Codeblock* block)
{
  file->blocks[file->items] = (*block);
  file->items++;

  if (file->items == file->size) {
    file->size *= 2;
    file->blocks = realloc(file->blocks, file->size*sizeof(Codeblock));
  }
}

void
appendtocodebase(Codebase* base, Codefile* file)
{
  base->files[base->items] = (*file);
  base->items++;

  if (base->items == base->size) {
    base->size *= 2;
    base->files = realloc(base->files, base->size*sizeof(Codefile));
  }
}

Codeblock*
getblockinfile(Codefile* file, char* id)
{
  int i;
  for (i = 0; i < file->items; i++) {
    if (strcmp(file->blocks[i].id, id) == 0) {
      return file->blocks + i;
    }
  }
  return NULL;
}

Codefile*
getfileinbase(Codebase* base, char* path)
{
  int i;

  for (i = 0; i < base->items; i++) {

    if (strcmp(base->files[i].path, path) == 0) {
      return base->files + i;
    }
  }
  return NULL;
}

void
computeindent(Codeblock *block)
{
  unsigned int min = -1;
  unsigned int cur;
  char *c = block->code;

  for (c = block->code; c; c++) {
    if (*c == ' ')
      cur++;
    else
      min = cur < min ? cur : min;
  }

  block->indent = min;

}

Codefile*
loadcode(char* filename)
{
  FILE *f = NULL, *blockfile = NULL;
  Codefile *file = NULL;
  Codeblock *block;
  int progress = 0, lines = 0;
  char c, *idbuf;

  if (!(f = fopen(filename, "r")))
      return NULL;

  file = malloc(sizeof(Codefile));
  file->path = filename;
  file->size = INITIAL_BUFFER_SIZE;
  file->items = 0;
  file->blocks = malloc(INITIAL_BUFFER_SIZE*sizeof(Codeblock));

  lines += scanuntil(f, config->block_start_prefix, NULL);
  while (!feof(f)) {
    lines += whitespace(f);
    idbuf = token(f);
    endline(f); lines++;
    block = malloc(sizeof(Codeblock));
    block->id = idbuf;
    block->line = lines;
    blockfile = open_memstream(&(block->code), &(block->size));
    lines += scanuntil(f, config->block_end, blockfile);
    fclose(blockfile);
    appendtocodefile(file, block);
    lines += scanuntil(f, config->block_start_prefix, NULL);
  }

  return file;
}

/* @ getblockinbase.header */
Codeblock*
getblockinbase(Codebase* base, char* path, char* id)
{
  Codefile* file;
  Codeblock* block;
  /* @ end */

  /* @getblockinbase.load */
  if (!(file = getfileinbase(base, path))) {
    if (!(file = loadcode(path))) {
      die("Error loading file %s\n", path);
      return NULL;
    }
    appendtocodebase(base, file);
  }
  /* @ end */

  /* @ getblockinbase.get */
  if (!(block = getblockinfile(file, id)))
    return NULL;
  return block;
}
/* @ end */

Command
getsubcommand(char* cmd)
{
  if (strcmp(cmd, "merge") == 0) return MERGE;
  if (strcmp(cmd, "blocks") == 0) return BLOCKS;
  return UNKNOWN;
}
/* ====================================================================== */

/* Subcommands ========================================================== */
/* @ merge.header */
int
merge(int argc, char** argv)
{
  unsigned int lines;
  char arg, *infile, *idbuf, *pathbuf;
  FILE *source;
  Codeblock *block;
  Codefile *file;
  Codebase base;
  /* @ end */
  /* @ merge.options */
  while ((arg = getopt(argc, argv, MERGE_ARG_STRING)) != -1) {
    switch(arg) {
    default:
      fprintf(stderr,"Unknown option %c\n", arg);
      break;
    }
  }

  if(!(infile = argv[optind])) {
    die("Missing source file argument.\n");
    return 1;
  }

  if (!(source = fopen(infile, "r"))) {
    die("Could not open source file.\n");
    return 1;
  }
  /* @ end */

  /* @ merge.base */
  base.size = INITIAL_BUFFER_SIZE;
  base.items = 0;
  base.files = malloc(base.size*sizeof(Codefile));
  /* @ end */

  /* @ merge.firstsearch */
  lines += scanuntil(source, config->insertion_point_marker, stdout);
  /* @ end */
  /* @ merge.while.parsing */
  while (!feof(source)) {
    lines += whitespace(source);
    idbuf = token(source);
    lines += whitespace(source);
    pathbuf = token(source);
    /* @ end */
    /* @ merge.while.block */
    if (!(block = getblockinbase(&base, pathbuf, idbuf))) {
      die("%s:%d: No block with id '%s' in file %s\n", infile, lines, idbuf, pathbuf);
      return 1;
    }
    fprintblock(stdout, block);
    lines += scanuntil(source, config->insertion_point_marker, stdout);
  }
  /* @ end */

  fclose(source);
  return 0;
}

int
blocks(int argc, char** argv)
{
  Codefile *file;
  char arg, *infile;

  while ((arg = getopt(argc, argv, BLOCKS_ARG_STRING)) != -1) {
    switch (arg) {
    default:
      die("Unknown option %c\n", arg);
      break;
    }
  }

  if (!(infile = argv[optind])) {
    die("Missing code file argument.\n");
    return 1;
  }

  file = loadcode(infile);
  fprintfile(stdout, file);

  return 0;
}
/* ====================================================================== */

/* @ main.header */
int
main(int argc, char** argv)
{
  char arg, *subcommand;
  char *configpath = CONFIG_FILE_NAME;
  FILE *configfile;
/* @ end */

  /* @ main.options.while */
  while ((arg = getopt(argc, argv, MAIN_ARG_STRING)) != -1) {
    switch(arg) {
      /* @ end */
      /* @ main.options.default */
    default:
      die("Unknown option '%c'\n", arg);
      return 1;
      /* @ end */
    }
  }

  /* @ main.openings */
  if (!(configfile = fopen(configpath, "r"))) {
    die("Could not find config file %s.\n", CONFIG_FILE_NAME);
    return 1;
  }

  if (!(config = parseconfig(configfile))) {
    die("Could not parse config file %s.\n", CONFIG_FILE_NAME);
    return 1;
  }

  if (!(subcommand = argv[optind])) {
    die("Missing subcommand.\n");
    return 1;
  }
  /* @ end */

  /* @ main.caveat */
  optind++;
  /* @ end */

  /* @ main.subcommands */
  switch(getsubcommand(subcommand)) {
  case MERGE:
    return merge(argc, argv);
    break;
  case BLOCKS:
    return blocks(argc, argv);
  default:
    die("Unknown command \"%s\"\n", subcommand);
    return 1;
  }
}
/* @ end */
